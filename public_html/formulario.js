window.onload= iniciar;

function iniciar(){
    //se definen variables globales
    
    var fondoPorDefecto;
    var idConFoco="nombre";
    var idPierdeFoco;
    
    
//    //Por defecto dejamos activo el botón de radio 'Hembra'
//    document.getElementById('opcion0').checked=true;
//    document.getElementById('hembra').style.backgroundColor="#ECFF33";
    //Almacenamos el fondo de los input
    fondoPorDefecto=document.getElementById('nombre').style.backgroundColor;

    //Añadimos el evento limpiarCampo al botón 'Limpiar campo', llamará a la función para limpiar el input que tenga el foco.
    document.getElementById('limpiarcampo').addEventListener('click',limpiacampo, false);
    //añadimos el evento enviar al botón 'Enviar', se llama a la función enviar donde se verifican los datos
    document.getElementById('enviar').addEventListener('click', enviar, false);
    //Añadimos el evento reiniciar al botón Reiniciar, llamara a la función reiniciar que borra todos los campos de los input
    document.getElementById('reiniciar').addEventListener('click', reiniciar, false);
    
    //añadimos los eventos onfocus a los objetos que tendrán foco
    document.getElementById('nombre').addEventListener('focus', cambiarFondo, false);
    document.getElementById('login').addEventListener('focus', cambiarFondo, false);
    document.getElementById('password1').addEventListener('focus', cambiarFondo, false);
    document.getElementById('password2').addEventListener('focus', cambiarFondo, false);
    document.getElementById('listado').addEventListener('focus', cambiarFondo, false);

    
    //añadimos los eventos onblur a los objetos que pierden el foco
    document.getElementById('nombre').addEventListener('blur', restaurarFondo, false);
    document.getElementById('login').addEventListener('blur', restaurarFondo, false);
    document.getElementById('password1').addEventListener('blur', restaurarFondo, false);
    document.getElementById('password2').addEventListener('blur', restaurarFondo, false);
    document.getElementById('listado').addEventListener('blur', restaurarFondo, false);

    
    //Añadimos el evento KeyUp para el input login
    document.getElementById('login').addEventListener('keyup', convertirAMayusculas, false);
    
    //Se limpian todos los campos de los input
    function reiniciar(){
          document.getElementById('nombre').style.backgroundColor="white";
          document.getElementById('nombre').value="";
          document.getElementById('login').style.backgroundColor="white";
          document.getElementById('login').value="";
          document.getElementById('password1').style.backgroundColor="white";
          document.getElementById('password1').value="";
          document.getElementById('password2').style.backgroundColor="white";
          document.getElementById('password2').value="";
          document.getElementById('listado').style.backgroundColor="white";
          document.getElementById('listado').value="";
          //deseleccionamos los radio button
          document.querySelectorAll('[name=sexo]').forEach((x) => x.checked=false);
          document.getElementById('resultado').innerHTML="";
    }
    
    //Se convierten a mayusculas mientras se escribe carácteres
    function convertirAMayusculas(){
        document.getElementById('login').value=document.getElementById('login').value.toUpperCase();
    }
        
    //Se limpia el input que tenga el foco, este será el último input seleccionado
    function limpiacampo(){
        //Utilizamos el elemento al que hacemos foco para borrar el campo
        if(idPierdeFoco==="sexo"){
           document.querySelectorAll('[name=sexo]').forEach((x) => x.checked=false); 
        }else{
        document.getElementById(idPierdeFoco).value='';
        }
    }
    
    //Cuando se selecciona un select se cambia el color del fondo
    function cambiarFondo(){
        //obtenemos e id del objeto que tiene el foco
        idConFoco=document.activeElement.name;
        document.getElementById(idConFoco).style.backgroundColor="#ECFF33";
        idPierdeFoco=idConFoco;
    }
    
    //Restauramos el fondo del elemento que ha perdido el foco
    function restaurarFondo(){
       document.getElementById(idConFoco).style.backgroundColor="#ffffff";
    }
    //Se verifican los datos introducidos, se muestran si no hay errores o se muestran los errores 
    function enviar(){
        //definición de variables
        // variable booleana que almacena el posible error en el nombre
        var errorDeNombre=false;
        var error=false;
        var string='DATOS INTRODUCIDOS CORRECTAMENTE<br>';
        var resultado='ERRORES ENCONTRADOS<br><br>';
        
        //se obtiene la longitud del Nombre Mascota.
        // Debe verificarse que el nombre solo dispone de caracteres alfabéticos y espacios para 
        // cuando el nombre es compuesto.  Al menos tres caracteres de mínimo y 20 de máximo.  
        // Es obligatorio y no se puede dejar en blanco.  Debe aparecer el texto en rojo cuando 
        // el número de caracteres no cumplan la longitud.
        var nombreIntroducido=document.getElementById('nombre').value;
        var longitud=nombreIntroducido.length;
        
        // (?=.{3,20}$)           --> Lo siguiente debe ser cualquier cosa entre 3 y 20 y luego fin de línea
        // [A-ZÑÁÉÍÓÚa-zñáéíóú]+ --> Se permiten mayúsculas y minúsculas para la primera palabra una o más letras
        // (?:                    --> Grupo
        // [A-ZÑÁÉÍÓÚa-zÑñáéíóú]+ --> Se permiten mayúsculas y minúsculas para la segunda palabra una o más letras
        // )?                     --> El grupo puede aparecer 0 o 1 vez
        
        var formatoNombreMascota=/^(?=.{3,20}$)[A-ZÑÁÉÍÓÚa-zñáéíóú]+(?: [A-ZÑÁÉÍÓÚa-zñáéíóú]+)?$/;
        
        //se compara el nombre de mascota con la expresión regular
        var testNombreMascota=formatoNombreMascota.test(nombreIntroducido);
        
        //se verifica la longitud del nombre introducido tenga una longitus adecuada
        if(longitud===0){
            resultado+='El nombre de mascota no puede estar en blanco.<br>';
            errorDeNombre=true;
            error=true;
        }
        //se verifica la longitud del nombre introducido tenga una longitus adecuada
        if((longitud<=3 || longitud>=20)&& !errorDeNombre){
            resultado+='El nombre de mascota tiene la longitud incorrecta, debes estar entre 3 y 20 carácteres.<br>';
            errorDeNombre=true;
            error=true;
        }
        
        //se verifica que el nombre de mascota no tenga errores
        if((testNombreMascota===false) && !errorDeNombre){
            resultado+='Verifica el formato del nombre<br>';
            errorDeNombre=true;
            error=true;
        
        }
        
        //si errorDeNombre=true hay error cambiamos el color de fondo de los input
        if(errorDeNombre){
            //fondo rojo para el aviso de error
            document.getElementById('nombre').style.backgroundColor='#FA0C0C';
        }else{
            //fondo verde si es correcto
            document.getElementById('nombre').style.backgroundColor=('#c1edb3');
            string+='Nombre: '+nombreIntroducido+"<br>";
        }
        
        //Formato de login
        //Debe verificarse que el nombre sólo dispone de caracteres alfabéticos en mayúscula (sin números)
        // y espacios. No puede tener más de 12 caracteres alfabéticos y no menos de 6.  Es obligatorio. 
        // Debe aparecer el texto en rojo cuando el número de caracteres no cumplan la longitud. A la hora 
        // de introducirlos caracteres alfabéticos deben convertirse en mayúscula automáticamente. 
        
        //  [A-ZÁÉÍÓÚ]   -->Sólo se permites letras mayusculas.
        //  {6,12}       -->Se permiten un mínimo de 6 y un máximmo de 12.
        var formatoLogin=/\b[A-ZÑÁÉÍÓÚ]{6,12}\b/;
      
        var loginIntroducido=document.getElementById('login').value;
        var longitudLogin=loginIntroducido.length;
          
        var errorDeLogin=false;
        //Comparamos el login introducido con la expresión regular si no coincide se devuelve false
        var testLogin=formatoLogin.test(loginIntroducido);
        
        if(longitudLogin===0){
            resultado+="El login no puede estar en blanco<br>";
            errorDeLogin=true;
            error=true;
        }
        //se verifica si está dentro de la longitud
        if((longitudLogin<6 || longitudLogin>12)&& !errorDeLogin) {
            resultado+="Revisa la longitud del login<br>";
            errorDeLogin=true;
            error=true;
        }
        
        //se verifica que el login no tenga errores
        if(testLogin===false && !errorDeLogin){
            resultado+='Verifica el formato del login<br>';
            errorDeLogin=true;
            error=true;
        }
        
        //si errorDeLogin=true hay error cambiamos el color de fondo del input
        if(errorDeLogin){
            //fondo rojo para el aviso de error
            document.getElementById('login').style.backgroundColor='#FA0C0C';
        }else{
            //fondo verde si es correcto
            document.getElementById('login').style.backgroundColor=('#c1edb3');
            string+='Login: '+loginIntroducido+"<br>";
        }
        //Contraseña
        //    Al menos 8 caracteres. 12 máximo. (?=^.{8,12}$)  
        //    Caracteres alfabéticos (al menos dos). (?=.*[a-záéíóúñA-ZÁÉÍÓÚÑ].*[a-záéíóúñA-ZÁÉÍÓÚÑ])  
        //    (?=.*[A-ZÁÉÍÓÚÑ]+)  Una mayúscula al menos.
        //    Puede contener de 1 a 3 dígitos. No se puede superar este límite. No tienen por que ser consecutivos los dígitos. 
        //    ((?:\D*\d){1,3}\D*$)
        //    Debe contener "." o  "," o  "-". (?!.*[\.\-\,]{2,})
   
        var formatoPassword=/^(?=^.{8,12}$)(?!.*[\.\-\,]{2,})(?=.*[a-záéíóúñA-ZÁÉÍÓÚÑ].*[a-záéíóúñA-ZÁÉÍÓÚÑ])(?=.*[A-ZÁÉÍÓÚÑ]?)((?:\D*\d){1,3}\D*$)$/;
        
         var password1Introducido=document.getElementById('password1').value;
         var password2Introducido=document.getElementById('password2').value;
        var longitudDePassword=password1Introducido.length;
        var errorDePassword=false;
        
        var testPassword1;
        
        if(longitudDePassword===0){
            resultado+="El password no puede estar en blanco<br>";
            errorDePassword=true;
            error=true;
        }
        
        //se verifica si está dentro de la longitud
        if((longitudDePassword<8 || longitudDePassword>12) && !errorDePassword){
            resultado+="Revisa la longitud del Password<br>";
            errorDePassword=true;
            error=true;
        }
        //Si los dos password son iguales y no hay error de longitud se continua con la verificación
        if((password1Introducido!==password2Introducido) && !errorDePassword){
             errorDePassword=true;
             error=true;
             resultado+="Las contraseñas no coinciden.";
         }
         
        
        //si no hay error depassword evaluamos la expresión regular
        if(!errorDePassword){
            testPassword1=formatoPassword.test(password1Introducido);
           
        }
        
        
        //se verifica que el Password no tenga errores
        if(testPassword1===false){
            resultado+='Verifica el formato del password:<br>'+
                        'Al menos 8 caracteres. 12 máximo. Caracteres alfabéticos (al menos dos).<br>' +
                        'Una mayúscula al menos.  Puede contener de 1 a 3 dígitos. Debe contener "." o  "," o  "-".<br>';
            errorDePassword=true;
            error=true;
        }
        
        //si errorDePassword=true hay error cambiamos el color de fondo del input
        if(errorDePassword){
            //fondo rojo para el aviso de error
            document.getElementById('password1').style.backgroundColor='#FA0C0C';
            document.getElementById('password2').style.backgroundColor='#FA0C0C';
        }else{
            //fondo verde si es correcto
            document.getElementById('password1').style.backgroundColor=('#c1edb3');
            document.getElementById('password2').style.backgroundColor=('#c1edb3');
            string+='Password: '+password1Introducido+"<br>";
        }
        
        //Se verifica si se ha seleccionado un valor del select
        var especieSeleccionada=document.getElementById('listado').value;
       
        if(especieSeleccionada===""){
            error=true;
            resultado+='Se tiene que seleccionar una especie.<br>';
            document.getElementById('listado').style.backgroundColor='#FA0C0C';
        }else{
            //fondo verde si es correcto
            document.getElementById('listado').style.backgroundColor=('#c1edb3');
            string+='Especie: '+especieSeleccionada+"<br>";
        }
        //Se verifica si hay sexo seleccionado
        if(!document.form.sexo[1].checked && !document.form.sexo[0].checked){
            error=true;
            resultado+='Se tiene que seleccionar un sexo.';
        }else{
            if(document.form.sexo[0].checked){
                string+="Sexo: Hembra";
            }else{
                string+="Sexo: Macho";
            }
        }
   
         //Se muestra el resultado en la capa de resultado
          document.getElementById('resultado').innerHTML=resultado;
          
          //si no hay errores se muestra resultado en negro de lo contrario en rojo
         if(error===false){
             document.getElementById('resultado').style.color="black";
             document.getElementById('resultado').innerHTML=string;
         }else{
              document.getElementById('resultado').style.color="red";
             document.getElementById('resultado').innerHTML=resultado;
         }
    }
}
