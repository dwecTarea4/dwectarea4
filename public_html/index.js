/* 
 * Tarea 4 DWEC Menú principal
 * Autor: Juan Francisco Vico Martínez
 */

//Cargamos la función 'iniciar' cuenado la página esté completamente cargada

window.onload=iniciar;
    
    //Se asignan los eventos click a los objetos botonFormulario y botonFormularioHtml5.
    //Se cargaran las correspondientes páginas y los eventos seran capturados en su fase de captura
    function iniciar(){
        botonFormulario.addEventListener('click', cargarFormulario, true);
        botonFormularioHtml5.addEventListener('click', cargarFormularioHtml5, true);
    }
    
    //Se abre al página formulario.htlm
    function cargarFormulario(){
        window.location.href='formulario.html';
    }
    
     //Se abre al página formularioHtml5.htlm
    function cargarFormularioHtml5(){
        window.location.href='formularioHtml5.html';
    }