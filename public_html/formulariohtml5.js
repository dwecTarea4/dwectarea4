//Definicion de correctoes
var correcto=0;
var incorrecto=0;

//variable que almacena los prticipantes
var numeroParticipantes=0;
// La primera función que se inicia cuando se carga la página.
window.onload=inicio;

//se capturan los botones y se lee el correcto
function inicio(){
    //esperamos a la escucha de los botones
    document.getElementById('limpiar').addEventListener('click', limpiarCampos, false);
    document.getElementById('enviar').addEventListener('click', enviar_datos, false);
    document.getElementById('borrarContadores').addEventListener('click', borrarStorage, false);
    
    //se crea un evento para cada tecla pulsada en el texarea="descripcion"
    document.getElementById('descripcion').addEventListener('keypress', verificar_textarea, false);
    //inicializamos los intentos
    borrarStorage();
    //Se muestran todos los intentos
    document.getElementById("correctos").innerHTML="Intentos correctos:  "+localStorage.correcto;
    document.getElementById("incorrectos").innerHTML="Intentos incorrectos: "+localStorage.incorrecto;
}
//Se lee correcto si no esta definido se le el valor de correcto+1 y se deja en 1
//si está definido se obtiene y se incrementa en 1
function actualiza_intentos(){
    //se obtiene el valor de intentos correctos si no esta definido se pone a 0
    if (localStorage.correcto===undefined){
        localStorage.correcto=0;
        document.getElementById("correctos").innerHTML="Intentos correctos: "+localStorage.correcto;

    }else{
        correcto=parseInt(localStorage.correcto);
        localStorage.correcto=correcto+1;
        document.getElementById("correctos").innerHTML="Intentos correctos: "+localStorage.correcto;
    }
 //se obtiene el valor de intentos incorrectos si no esta definido se pone a 0
    if (localStorage.incorrecto===undefined){
        localStorage.incorrecto=0;
        document.getElementById("incorrectos").innerHTML="Intentos incorrectos: "+localStorage.incorrecto;

    }else{
        incorrecto=parseInt(localStorage.incorrecto);
        localStorage.incorrecto=incorrecto+1;
        document.getElementById("incorrectos").innerHTML="Intentos incorrectos: "+localStorage.incorrecto;
    }
}
//se borra el valor de correcto e incorrecto a 0 y se actualiza en pantalla
function borrarStorage(){
    localStorage.correcto=0;
    document.getElementById("correctos").innerHTML="Intentos correctos: "+localStorage.correcto;
    localStorage.incorrecto=0;
    document.getElementById("incorrectos").innerHTML="Intentos incorrectos: "+localStorage.correcto;

}

//Se verifica que cada caracter ingresado cumple el patrón 
function verificar_textarea(e){
    //se comprueva mediane la expresión regular que el caracter está permitido
    //Se permiten caracteres especiales, caracteres alfanuméricos y espacios
    if (e.key.match(/[?¿!¡,\.;\w\s]/)===null){
            //Si el caracter introducido no es correcto se elimina
            e.preventDefault();
            document.getElementById("descripcion").style.backgroundColor="#edc1b3";
    }else{
    descripcion.style.backgroundColor="#f3f3f3";
    }
}
 

//Borrar todos los campos{
function limpiarCampos(){
    document.getElementById('resultado').innerHTML="";
    document.getElementById('descripcion').value="";
    document.getElementById('tipo_competicion').value="";
    document.getElementById('listado').value="";
}


//Se verifica los compos tipo y arma
function enviar_datos(){

    var error="";

    actualiza_intentos();
    //se verifica que se cumple el patrón en el input

    document.getElementById('tipo_competicion').pattern;
    var competicion=document.getElementById('tipo_competicion').value;

    if(arma_seleccionada===""){
        error+="Se tiene que especificar un tipo de arma.<br>";
    }

    if(competicion.length<6 || competicion.length>40){
        error+="La longitud de la competición tiene que estar entre 6 y 40 carácteres.<br>";
    }
    //si error tiene errores acumulados lo mostramos
    if(error.length>0){
       document.getElementById('resultado').style.color="red";
        document.getElementById('resultado').innerHTML="ERRORES ETECTADOS<br><br>"+error;
    }else{
        var areatexto=document.getElementById("descripcion").value;

        document.getElementById('resultado').style.color="black";
        document.getElementById('resultado').innerHTML=("DATOS ENVIADOS CORRECTAMENTE:<br><br>"+
                             "Tipo de competición: "+competicion+"<br>"+
                              "Descripción: "+areatexto+"<br>"+
                              "Arma seleccionada: "+arma_seleccionada+"<br>");
    }  

}


